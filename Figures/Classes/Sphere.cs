﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    class Sphere : Body
    {
        private double Radius { get; set; }
        protected override void CalculateSurface()
        {
            Volume = 4f / 3f * Math.PI * Math.Pow(Radius, 3);
        }

        protected override void CalculateVolume()
        {
            Surface = 4 * Math.PI * Math.Pow(Radius, 2);
        }
        /// <summary>
        /// Creates sphere
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="color">Color</param>
        /// <param name="radius">Radius</param>
        public Sphere (string name, string color, double radius)
        {
            Name = name;
            Color = color;
            Radius = radius;

            CalculateSurface();
            CalculateVolume();
        }
    }
}

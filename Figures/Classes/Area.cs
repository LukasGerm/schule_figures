﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    public abstract class Area : Figure
    {
        /// <summary>
        /// Scope of the area
        /// </summary>
        protected double Scope { get; set; }
        /// <summary>
        /// Surface of the area
        /// </summary>
        protected double Surface { get; set; }
        /// <summary>
        /// calculates the surface
        /// </summary>
        protected abstract void CalculateSurface();
        /// <summary>
        /// Calculates scope
        /// </summary>
        protected abstract void CalculateScope();
        /// <summary>
        /// Creates nice string
        /// </summary>
        /// <returns>The created string</returns>
        public override string ToString()
        {
            return "Diese Fläche mit dem Namen " + Name + " in der Farbe " + Color
                + " hat einen Flächeninhalt von " + Math.Round(Surface,2)
                + "cm^2 und einen Umfang von " + Math.Round(Scope,2) + "cm";
        }
    }
}

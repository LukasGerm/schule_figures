﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    public abstract class Body : Figure
    {
        /// <summary>
        /// Volume of the Body
        /// </summary>
        protected double Volume { get; set; }
        /// <summary>
        /// Surface of the body
        /// </summary>
        protected double Surface { get; set; }
        /// <summary>
        /// Calculates volume, is abstract
        /// because it should not be called in the base class
        /// </summary>
        protected abstract void CalculateVolume();
        /// <summary>
        /// Calculates surface
        /// </summary>
        protected abstract void CalculateSurface();
        /// <summary>
        /// Creates nice string
        /// </summary>
        /// <returns>The created string</returns>
        public override string ToString()
        {
            return "Dieser Körper mit dem Namen "+Name+ " in der Farbe "+Color
                + " hat ein Volumen von "+ Math.Round(Volume, 2)
                + "cm^3 und einen Grundflächeninhalt von "+ Math.Round(Surface,2) + "cm^2";
        }
    }
}

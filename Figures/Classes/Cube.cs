﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    public class Cube : Body
    {

        /// <summary>
        /// Primary site of the square
        /// </summary>
        private double SiteA { get; set; }
        /// <summary>
        /// Calculates surface of the square
        /// </summary>
        protected override void CalculateSurface()
        {
            Surface = (SiteA * SiteA) * 6;
        }
        /// <summary>
        /// Calculates volume of square
        /// </summary>
        protected override void CalculateVolume()
        {
            Volume = SiteA * 3;
        }
        /// <summary>
        /// Creates Cube
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="color">Color</param>
        /// <param name="siteA">Site A length in cm </param>
        public Cube(string name, string color, double siteA)
        {
            Name = name;
            Color = color;
            SiteA = siteA;
            //Calculate on construction
            CalculateSurface();
            CalculateVolume();
        }
    }
}

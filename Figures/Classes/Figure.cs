﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    /// <summary>
    /// Base class for figures
    /// </summary>
    public abstract class Figure
    {
        /// <summary>
        /// Figure color
        /// </summary>
        protected string Color { get; set; }
        /// <summary>
        /// Name of that figure
        /// </summary>
        protected string Name { get; set; }


    }
}

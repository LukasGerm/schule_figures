﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{

    class Cuboid : Body
    {
        private double SiteA { get; set; }
        private double SiteB { get; set; }
        private double SiteC { get; set; }
        protected override void CalculateSurface()
        {
            Surface = SiteA * SiteB * 2 + SiteB * SiteC * 2 + SiteC * SiteA * 2;
        }

        protected override void CalculateVolume()
        {
            Volume = SiteA * SiteB * SiteC;
        }
        /// <summary>
        /// Creates cuboid
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="color">Color</param>
        /// <param name="siteA">X site</param>
        /// <param name="siteB">Y site</param>
        /// <param name="siteC">Z site</param>
        public Cuboid(string name, string color, double siteA, double siteB, double siteC)
        {
            Name = name;
            Color = color;
            SiteA = siteA;
            SiteB = siteB;
            SiteC = siteC;

            CalculateSurface();
            CalculateVolume();
        }
    }
}

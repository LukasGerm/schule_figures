﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    class Rectangle : Area
    {
        /// <summary>
        /// The one site of the rectangle
        /// </summary>
        private double SiteA { get; set; }
        /// <summary>
        /// the second site of the rectangle
        /// </summary>
        private double SiteB { get; set; }
        protected override void CalculateScope()
        {
            Scope = SiteA * 2 + SiteB * 2;
        }

        protected override void CalculateSurface()
        {
            Surface = SiteA * SiteB;
        }
        /// <summary>
        /// Creates rectangle object
        /// </summary>
        /// <param name="name">Name of the Object</param>
        /// <param name="color">Color</param>
        /// <param name="siteA">The one Site</param>
        /// <param name="siteB">The other Site</param>
        public Rectangle(string name, string color, double siteA, double siteB)
        {
            Name = name;
            Color = color;
            SiteA = siteA;
            SiteB = siteB;

            CalculateScope();
            CalculateSurface();
        }
    }
}

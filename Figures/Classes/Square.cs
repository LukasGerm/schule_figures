﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    class Square : Area
    {
        private double SiteA { get; set; }
        /// <summary>
        /// Calculates scope
        /// </summary>
        protected override void CalculateScope()
        {
            Scope = 4 * SiteA;
        }
        /// <summary>
        /// Calculates surface
        /// </summary>
        protected override void CalculateSurface()
        {
            Surface = 2 * SiteA;
        }
        /// <summary>
        /// Creates Square
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="color">Color</param>
        /// <param name="siteA">The one site length</param>
        public Square(string name, string color, double siteA)
        {
            Name = name;
            Color = color;
            SiteA = siteA;

            CalculateScope();
            CalculateSurface();
        }
    }
}

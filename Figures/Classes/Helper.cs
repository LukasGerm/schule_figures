﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    /// <summary>
    /// Helper class has some 
    /// good helpers
    /// </summary>
    public class Helper
    {
        public static List<string> Colors = new List<string> { "red", "blue", "green", "orange" };
        /// <summary>
        /// enum where all figures are listed
        /// </summary>
        public enum Figures
        {
            Cube = 1,
            Square = 2,
            Cuboid= 3,
            Rectangle = 4,
            Sphere = 5,
            Circle = 6
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Classes
{
    class Circle : Area
    {
        private double Radius { get; set; }
        protected override void CalculateSurface()
        {
            Surface = Math.PI * Math.Pow(Radius, 2);
        }

        protected override void CalculateScope()
        {
            Scope = 2 * Math.PI * Radius;
        }
        /// <summary>
        /// Creates sphere
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="color">Color</param>
        /// <param name="radius">Radius</param>
        public Circle(string name, string color, double radius)
        {
            Name = name;
            Color = color;
            Radius = radius;

            CalculateSurface();
            CalculateScope();
        }

        
    }
}

﻿using Figures.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures
{
    class Program
    {
        static void Main(string[] args)
        {
            //the different figures from enum
            string figures = string.Join(",", Enum.GetNames(typeof(Helper.Figures)));
            while (true)
            {
                Console.WriteLine("Bitte wählen Sie eine Figur aus folgenden aus: " + figures);

                Helper.Figures chosenFigure;
                string name;
                string color;
                //loop for inputting the chosen figure
                while (true)
                {
                    //get type of figure
                    if (!Enum.TryParse(Console.ReadLine(), out chosenFigure))
                    {
                        Console.WriteLine("Dies ist kein valider Typ");
                    }
                    else break;
                }
                Console.WriteLine("Bitte geben Sie einen Namen für die Figur an");
                //read in the name
                name = Console.ReadLine();

                while (true)
                {
                    Console.WriteLine("Bitte geben Sie eine Fabre aus folgenden Farben an "+ String.Join(",", Helper.Colors));
                    color = Console.ReadLine();
                    if (!Helper.Colors.Exists(c => c == color))
                        Console.WriteLine("Dies ist keine valide Farbe");
                    else break;
                        
                }
                
                //some variables needed in the switch
                double siteA;
                double siteB;
                double siteC;
                double radius;
                //do the right things for the chosen figure
                switch (chosenFigure)
                {
                    case Helper.Figures.Cube:
                        while (true)
                        {
                            Console.WriteLine("Bitte geben Sie die Länge der Seite an in cm");

                            if (!double.TryParse(Console.ReadLine(), out siteA))
                                Console.WriteLine("Dies ist keine valide Nummer");

                            else break;

                        }
                        Console.WriteLine(new Cube(name, color, siteA).ToString());

                        break;
                    case Helper.Figures.Square:
                        while (true)
                        {
                            Console.WriteLine("Bitte geben Sie die Länge der Seite an in cm");

                            if (!double.TryParse(Console.ReadLine(), out siteA))
                                Console.WriteLine("Dies ist keine valide Nummer");

                            else break;

                        }
                        Console.WriteLine(new Square(name,color, siteA).ToString());
                        break;
                    case Helper.Figures.Rectangle:
                        // read in different values
                        Console.WriteLine("Bitte geben Sie die Länge von Seite A und B hintereinander ein und bestätigen Sie mit Enter");
                        while (true)
                        {
                            if (!double.TryParse(Console.ReadLine(), out siteA))
                                Console.WriteLine("Dies ist keine valide Nummer");
                            else break;
                                
                        }
                        while (true)
                        {
                            if (!double.TryParse(Console.ReadLine(), out siteB))
                                Console.WriteLine("Dies it keine valide Nummer");
                            else break;

                        }
                        Console.WriteLine(new Rectangle(name, color, siteA, siteB));
                        break;
                    case Helper.Figures.Cuboid:
                        Console.WriteLine("Bitte geben Sie die Länge von Seite A, B und C hintereinander ein und bestätigen Sie mit Enter");
                        while (true)
                        {
                            if (!double.TryParse(Console.ReadLine(), out siteA))
                                Console.WriteLine("Dies ist keine valide Nummer");
                            else break;

                        }
                        while (true)
                        {
                            if (!double.TryParse(Console.ReadLine(), out siteB))
                                Console.WriteLine("Dies it keine valide Nummer");
                            else break;

                        }
                        while (true)
                        {
                            if (!double.TryParse(Console.ReadLine(), out siteC))
                                Console.WriteLine("Dies it keine valide Nummer");
                            else break;

                        }
                        Console.WriteLine(new Cuboid(name, color, siteA, siteB, siteC));
                        break;
                    case Helper.Figures.Sphere:
                        while (true)
                        {
                            Console.WriteLine("Bitte geben Sie den Radius ein");
                            if (!double.TryParse(Console.ReadLine(), out radius))
                                Console.WriteLine("Dies ist keine valide Nummer");
                            else break;
                        }
                        Console.WriteLine(new Sphere(name, color, radius));
                        break;
                    case Helper.Figures.Circle:
                        while (true)
                        {
                            Console.WriteLine("Bitte geben Sie den Radius ein");
                            if (!double.TryParse(Console.ReadLine(), out radius))
                                Console.WriteLine("Dies ist keine valide Nummer");
                            else break;
                        }
                        Console.WriteLine(new Circle(name, color, radius));
                        break;
                }
                Console.WriteLine("Wollen Sie nicht noch eine Figur berechnen? Drücken Sie 'N' um das Programm zu beenden und 'Enter' um fortzufahren");
                
                // check if the user wants to exit the program
                if (Console.ReadLine().ToLower() == "n")
                    break;
            }
            
        }
    }
}
